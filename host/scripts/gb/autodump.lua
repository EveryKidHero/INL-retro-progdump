
-- create the module's table
local mbc1 = {}

-- import required modules
local dict = require "scripts.app.dict"
local dump = require "scripts.app.dump"
local flash = require "scripts.app.flash"
local files = require "scripts.app.files"

-- file constants
local mapname = "MBC1"



local cartTable ={
		  ['00'] = 'ROM ONLY',
		  ['01'] = 'MBC1',
		  ['02'] = 'MBC1 + RAM',
		  ['03'] = 'MBC1 + RAM + BATTERY',
		  ['05'] = 'MBC2',
		  ['06'] = 'MBC2 + BATTERY',
		  ['08'] = 'ROM + RAM',
		  ['09'] = 'ROM + RAM + BATTERY',
		  ['0B'] = 'MMM01',
		  ['0C'] = 'MMM01 + RAM',
		  ['0D'] = 'MMM01 + RAM + BATTERY',
		  ['0F'] = 'MBC3 + TIMER + BATTERY',
		  ['10'] = 'MBC3 + TIMER + RAM + BATTERY',
		  ['11'] = 'MBC3',
		  ['12'] = 'MBC3 + RAM',
		  ['13'] = 'MBC3 + RAM + BATTERY',
		  ['19'] = 'MBC5',
		  ['1A'] = 'MBC5 + RAM',
		  ['1B'] = 'MBC5 + RAM + BATTERY',
		  ['1C'] = 'MBC5 + RUMBLE',
		  ['1D'] = 'MBC5 + RUMBLE + RAM',
		  ['1E'] = 'MBC5 + RUMBLE + RAM + BATTERY',
		  ['20'] = 'MBC6',
		  ['22'] = 'MBC7+SENSOR+RUMBLE+RAM+BATTERY',
		  ['FC'] = 'POCKET CAMERA',
		  ['FD'] = 'BANDAI TAMA5',
		  ['FE'] = 'HuC3',
		  ['FF'] = 'HuC1+RAM+BATTERY',
		}

local licencees = {
['00'] = 'none',
['09'] = 'hot-b',
['0C'] = 'elite systems',
['19'] = 'itc entertainment',
['1F'] = 'virgin',
['25'] = 'san-x',
['30'] = 'infogrames',
['33'] = 'Possibly Super Gameboy supported',
['38'] = 'Capcom',
['3E'] = 'gremlin',
['44'] = 'Malibu',
['49'] = 'irem',
['4F'] = 'u.s. gold',
['52'] = 'activision',
['55'] = 'park place',
['59'] = 'milton bradley',
['5C'] = 'naxat soft',
['61'] = 'virgin',
['6E'] = 'elite systems',
['71'] = 'Interplay',
['75'] = 'the sales curve',
['7A'] = 'triffix entertainment',
['80'] = 'misawa entertainment',
['8B'] = 'bullet-proof software',
['8F'] = 'i`max',
['93'] = 'tsuburava',
['97'] = 'kaneko',
['9B'] = 'Tecmo',
['9F'] = 'nova',
['A4'] = 'Konami',
['A9'] = 'technos japan',
['AD'] = 'toho',
['B1'] = 'ascii or nexoft',
['B6'] = 'HAL',
['BA'] = '*culture brain o',
['BF'] = 'sammy',
['C3'] = 'Squaresoft',
['C6'] = 'tonkin house',
['CA'] = 'ultra',
['CD'] = 'meldac',
['D0'] = 'Taito',
['D3'] = 'sigma enterprises',
['D7'] = 'copya systems',
['DB'] = 'ljn',
['DF'] = 'altron',
['E2'] = 'uutaka',
['E7'] = 'athena',
['EA'] = 'king records',
['EE'] = 'igs',
['FF'] = 'ljn',
['01'] = 'nintendo',
['0A'] = 'jaleco',
['13'] = 'electronic arts',
['1A'] = 'yanoman',
['20'] = 'KSS',
['28'] = 'kotobuki systems',
['31'] = 'nintendo',
['34'] = 'konami',
['39'] = 'Banpresto',
['41'] = 'Ubisoft',
['46'] = 'angel',
['4A'] = 'virgin',
['50'] = 'absolute',
['53'] = 'american sammy',
['56'] = 'ljn',
['5A'] = 'mindscape',
['5D'] = 'tradewest',
['67'] = 'ocean',
['6F'] = 'electro brain',
['72'] = 'broderbund',
['78'] = 't*hq',
['7C'] = 'microprose',
['83'] = 'lozc',
['8C'] = 'vic tokai',
['91'] = 'chun soft',
['95'] = 'varie',
['99'] = 'arc',
['9C'] = 'imagineer',
['A1'] = 'Hori electric',
['A6'] = 'kawada',
['AA'] = 'broderbund',
['AF'] = 'Namco',
['B2'] = 'Bandai',
['B7'] = 'SNK',
['BB'] = 'Sunsoft',
['C0'] = 'Taito',
['C4'] = 'tokuma shoten intermedia',
['C8'] = 'koei',
['CB'] = 'vap',
['CE'] = '*pony canyon or',
['D1'] = 'sofel',
['D4'] = 'ask kodansha',
['D9'] = 'Banpresto',
['DD'] = 'ncs',
['E0'] = 'jaleco',
['E3'] = 'varie',
['E8'] = 'asmik',
['EB'] = 'atlus',
['F0'] = 'a wave',
['08'] = 'capcom',
['0B'] = 'coconuts',
['18'] = 'hudsonsoft',
['1D'] = 'clary',
['24'] = 'pcm complete',
['29'] = 'seta',
['32'] = 'bandai',
['35'] = 'hector',
['3C'] = '*entertainment i',
['42'] = 'Atlus',
['47'] = 'spectrum holoby',
['4D'] = 'malibu',
['51'] = 'acclaim',
['54'] = 'gametek',
['57'] = 'matchbox',
['5B'] = 'romstar',
['60'] = 'titus',
['69'] = 'electronic arts',
['70'] = 'Infogrammes',
['73'] = 'sculptered soft',
['79'] = 'accolade',
['7F'] = 'kemco',
['86'] = 'tokuma shoten intermedia',
['8E'] = 'ape',
['92'] = 'video system',
['96'] = 'yonezawa/s`pal',
['9A'] = 'nihon bussan',
['9D'] = 'Banpresto',
['A2'] = 'Bandai',
['A7'] = 'takara',
['AC'] = 'Toei animation',
['B0'] = 'Acclaim',
['B4'] = 'Enix',
['B9'] = 'pony canyon',
['BD'] = 'Sony imagesoft',
['C2'] = 'Kemco',
['C5'] = 'data east',
['C9'] = 'ufl',
['CC'] = 'use',
['CF'] = 'angel',
['D2'] = 'quest',
['D6'] = 'naxat soft',
['DA'] = 'tomy',
['DE'] = 'human',
['E1'] = 'towachiki',
['E5'] = 'epoch',
['E9'] = 'natsume',
['EC'] = 'Epic/Sony records',
['F3'] = 'extreme entertainment',

}

local ramTable ={
		  ['00'] = 'None',
		  ['01'] = '2 KBytes',
		  ['02'] = '8 KBytes',
		  ['03'] = '32 KBytes (4 banks of 8KB)',
		  ['04'] = '128 KBytes (16 banks of 8KB)',
		  ['05'] = '64 KBytes (8 banks of 8KB)',
		}

local romTable ={
		  ['00'] = '32 KByte (no ROM banking)',
		  ['01'] = '64 KBytes (4 banks)',
		  ['02'] = '128 KBytes (8 banks)',
		  ['03'] = '256 KBytes (16 banks)',
		  ['04'] = '512 KBytes (32 banks)',
		  ['05'] = '1 MBytes (64 banks)',
		  ['06'] = '2 MBytes (128 banks)',
		  ['07'] = '4 MBytes (256 banks)',
		  ['08'] = '8 MBytes (512 banks)',
		  ['52'] = '1.1 MBytes (72 banks)',
		  ['53'] = '1.2 MBytes (80 banks)',
		  ['54'] = '1.5 MBytes (96 banks)',
		}

local romBankTable ={
		  ['00'] = 2,
		  ['01'] = 4,
		  ['02'] = 8,
		  ['03'] = 16,
		  ['04'] = 32,
		  ['05'] = 64,
		  ['06'] = 128,
		  ['07'] = 256,
		  ['08'] = 512,
		  ['52'] = 72,
		  ['53'] = 80,
		  ['54'] = 96,
		}

local romSizeByte
		
-- local functions

local function unsupported(operation)
	print("\nUNSUPPORTED OPERATION: \"" .. operation .. "\" not implemented yet for Gameboy - ".. mapname .. "\n")
end

--dump the ROM
local function dump_rom( file, rom_size_KB, debug )


	--ROM ONLY dump all 32KB, most of this code is overkill for no MBC.
	--	but follows same format as MBC's
	local KB_per_read = 16	--read half the ROM space (16KByte)
	local num_reads = romBankTable[romSizeByte]--rom_size_KB / KB_per_read
	local read_count = 0
	local addr_base = 0x00	-- $0000 base address for ROM
    print('reading ', num_reads, ' banks')
	--the first bank is fixed & only visible at $0000-3FFF
	
	--if debug then print( "dump ROM part ", read_count, " of ", num_reads) end
	--dump.dumptofile( file, 32, addr_base, "GAMEBOY_PAGE", false )
	--read_count = 1
	
	--remaining banks must be read from $4000-7FFF
	--addr_base = 0x40
	--banks 0x20, 0x40, 0x60 are not visible, they present 0x21, 0x41, 0x61 instead
	--much like how 0x00 would present 0x01 at $4000-7FFF
	--so there's a max of 125 banks because of these 3 lost banks.. (almost 2MByte)
	--this doesn't affect roms that are 512KByte or less because they only
	--use mapper bits 5-0, and bits 6 & 7 are the ones that are affected by this.

	local mybank=0x2000
	while ( read_count < num_reads ) do

		if (read_count > 0) then
		  addr_base = 0x40
		end
		KB_per_read=16
		
        --mbc2 chips need to be written to highbit 1 to change banks		
		dict.gameboy("GAMEBOY_WR", 0x2100, read_count)
		print ("reading from rom bank: ",read_count)

		if debug then print( "dump ROM part ", read_count, " of ", num_reads) end

		dump.dumptofile( file, KB_per_read, addr_base, "GAMEBOY_PAGE", false )

		read_count = read_count + 1
	end

end

function seq_read(base_addr, n)
    local rv = {}
    local count = 0
    while (count < n) do
        local val = dict.gameboy("GAMEBOY_RD", base_addr + count)
        count = count + 1
        -- Kind of an ordering hack because Lua likes 1-based structures.
        rv[count] = val
    end
    return rv
end


oct2bin = {
 ['0'] = '000',
 ['1'] = '001',
 ['2'] = '010',
 ['3'] = '011',
 ['4'] = '100',
 ['5'] = '101',
 ['6'] = '110',
 ['7'] = '111',
}
function getOct2bin(a) return oct2bin[a] end
function toBits(n)
  local s = string.format('%.o', n)
  s=s:gsub('.', getOct2bin)
  
  if(s=='')then
    s='00000000'
  end
  return string.format("%08d", s)
end

-- Cart should be in reset state upon calling this function 
-- this function processes all user requests for this specific board/mapper
local function process(process_opts, console_opts)
	local file 

	-- Initialize device i/o for Gameboy
	dict.io("IO_RESET")
	dict.io("GAMEBOY_INIT")

	dict.io("GB_POWER_5V")	-- Gameboy carts prob run fine at 3v if want to be safe

	-- TODO: test the cart
	if process_opts["test"] then
		print("no test function yet")
		--unsupported("test")
	end

	-- Dump the cart to dumpfile
	--if process_opts["read"] then
	--we're always dumping here.
	
		local byte_table = seq_read(0x0100, 335)
		for _, byteVal in ipairs(byte_table) do
		  --io.write (string.format('%02X', byteVal), " ")
		  io.write (string.char(byteVal), " ")
		end
		
		io.write("\n\nTitle: ")
		local count = 1
        while (count < 17) do
		  io.write (string.char(byte_table[0x0134 - 0x0100 +count]))
		  count = count+1
        end
		
		io.write("\nManufacturer: ")
		local count = 1
        while (count < 5) do
		  io.write (string.format('%02X',byte_table[0x013F - 0x0100 +count]))
		  count = count+1
        end
		
		io.write("\nGB/GBC: ")
    	  if (byte_table[0x0143 - 0x0100 +1] == 0xC0) then
			io.write("GAMEBOY COLOR")
		  else
		   io.write("GAMEBOY")
		  end
		
		
		io.write("\nLicensee: ")
		local count = 1
        while (count < 3) do
		  io.write (string.char(byte_table[0x0144 - 0x0100 +count]))
		  --io.write (string.format('%02X',byte_table[0x0144 - 0x0100 +count]))
		  --io.write (byte_table[0x0144 - 0x0100 +count])
		  
		  count = count+1
        end
		
		io.write("\nSuper GB Enhanced: ")
		local sgbByte = string.format('%02X',byte_table[0x0146 - 0x0100 +1])
		if ( sgbByte == "03" ) then
		  io.write("yes")
		else
		  io.write ("no")
		end
		io.write (' (0x',sgbByte,')')
		
		
		
		
		io.write("\nCart type: ")
		local cartTypeByte = string.format('%02X',byte_table[0x0147 - 0x0100 +1])
		io.write('0x', cartTypeByte, ' ', cartTable[cartTypeByte])  
		
		
		
		io.write("\nROM Size: ")
		romSizeByte = string.format('%02X',byte_table[0x0148 - 0x0100 +1])
		io.write('0x', romSizeByte, ' ', romTable[romSizeByte])
		
		
		
		io.write("\nRAM Size: ")
		local ramSizeByte = string.format('%02X',byte_table[0x0149 - 0x0100 +1])
		io.write('0x', ramSizeByte, ' ',ramTable[ramSizeByte])
		
		
		--0x014A Destination Japan or "other" we don't really care
		io.write("\nDestination: ")
		local destByte = string.format('%02X',byte_table[0x014A - 0x0100 +1])
		if ( destByte == "00" ) then
		  io.write("Japan")
		else
		  io.write ("non-Japan")
		end
		io.write (' (0x',destByte,')')
		
		
		io.write("\nOld Licensee Code: ")
		local newLicByte = string.format('%02X', byte_table[0x014B - 0x0100 +1])
		if ( newLicByte == "33" ) then
		  io.write("no, see Licensee")
		  io.write (' (0x',newLicByte,')')
		else
		  io.write ("yes")
		  io.write (' (0x',newLicByte,')')
		  io.write (' ' , licencees[newLicByte])
		  
		end
		
		
		
		io.write("\nRom Version: ")
		local count = 1
        while (count < 2) do
		  io.write (string.format('%02X',byte_table[0x014C - 0x0100 +count]))
		  --io.write (string.format('%02X',byte_table[0x0147 - 0x0100 +count]))
		  count = count+1
        end
		
		io.write("\nHeader Checksum: ")
		local count = 1
        while (count < 2) do
		  io.write (string.format('%02X',byte_table[0x014D - 0x0100 +count]))
		  io.write (' ', byte_table[0x014D - 0x0100 +count], ' ')
		  io.write (toBits(byte_table[0x014D - 0x0100 +count]))
		  --io.write (string.format('%02X',byte_table[0x0147 - 0x0100 +count]))
		  count = count+1
        end
		
		io.write("\nGlobal Checksum: ")
		local count = 1
        io.write (string.format('%02X',byte_table[0x014E - 0x0100 + 1]))
		io.write (string.format('%02X',byte_table[0x014F - 0x0100 + 1]))
		io.write (' ')
		io.write(toBits(byte_table[0x014E - 0x0100 + 1]))
		io.write(' ')
		io.write (toBits(byte_table[0x014F - 0x0100 + 1]))
		
		
		print("\nDumping ROM...")
		
		file = assert(io.open(process_opts["dump_filename"], "wb"))
        
		-- Dump cart into file
		dump_rom(file, console_opts["rom_size_kbyte"], false)
        
		-- Close file
		assert(file:close())
		print("DONE Dumping ROM")
	--end


	-- TODO: erase the cart
	if process_opts["erase"] then
		unsupported("erase")
	end


	-- TODO: program flashfile to the cart
	if process_opts["program"] then
		unsupported("program")
	end

	-- Verify flashfile is on the cart
	-- (This is sort of pointless until "program" is supported)
	if process_opts["verify"] then
		--for now let's just dump the file and verify manually
		print("\nPost dumping ROM...")

		file = assert(io.open(process_opts["verify_filename"], "wb"))

		--dump cart into file
		dump_rom(file, console_opts["rom_size_kbyte"], false)

		--close file
		assert(file:close())

		print("DONE post dumping ROM")
		if (files.compare( process_opts["verify_filename"], "ignore/madden96_bank0.gb", true ) ) then
			print("\nSUCCESS! Flash verified")
		else
			print("\n\n\n FAILURE! Flash verification did not match")
		end
	end

	dict.io("IO_RESET")
end

-- global variables so other modules can use them
--    NONE

-- call functions desired to run when script is called/imported
--    NONE

-- functions other modules are able to call
mbc1.process = process

-- return the module's table
return mbc1
