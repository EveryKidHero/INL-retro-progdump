
-- create the module's table
local gxrom = {}

-- import required modules
local dict = require "scripts.app.dict"
local nes = require "scripts.app.nes"
local dump = require "scripts.app.dump"
local flash = require "scripts.app.flash"
local swim = require "scripts.app.swim"
local ciccom = require "scripts.app.ciccom"
local buffers = require "scripts.app.buffers"

-- file constants
local mapname = "GxROM"

-- local functions

local function create_header( file, prgKB, chrKB )
	local mirroring = nes.detect_mapper_mirroring()

	--write_header( file, prgKB, chrKB, mapper, mirroring )
	nes.write_header( file, prgKB, chrKB, op_buffer[mapname], mirroring)
end

--dump the PRG ROM
local function dump_prgrom( file, rom_size_KB, debug )
	assert(rom_size_KB <= 128) -- maximum available space in PRG for GxROM is 128KB

	local KB_per_read = 32
	-- Handle 16KB nroms.
	if rom_size_KB < KB_per_read then KB_per_read = rom_size_KB end

	local num_reads = rom_size_KB / KB_per_read
	local read_count = 0
	local addr_base = 0x08	-- $8000

	while ( read_count < num_reads ) do
		-- Bits 4 and 5 control the PRG ROM bank
		dict.nes("NES_CPU_WR", 0x8000, (read_count << 4))

		if debug then print( "dump PRG part ", read_count, " of ", num_reads) end

		dump.dumptofile( file, KB_per_read, addr_base, "NESCPU_4KB", false )

		read_count = read_count + 1
	end
end

--dump the CHR ROM
local function dump_chrrom( file, rom_size_KB, debug )
	assert(rom_size_KB <= 32) -- maximum available space in CHR for GxROM is 32KB

	local KB_per_read = 8	
	local num_reads = rom_size_KB / KB_per_read
	local read_count = 0
	local addr_base = 0x00	-- $0000

	while ( read_count < num_reads ) do
		-- Bits 0 and 1 control the CHR ROM bank
		dict.nes("NES_CPU_WR", 0x8000, read_count)

		if debug then print( "dump CHR part ", read_count, " of ", num_reads) end

		dump.dumptofile( file, KB_per_read, addr_base, "NESPPU_1KB", false )

		read_count = read_count + 1
	end
end

--Cart should be in reset state upon calling this function 
--this function processes all user requests for this specific board/mapper
local function process(process_opts, console_opts)
	local test = process_opts["test"]
	local read = process_opts["read"]

	local rv = nil
	local file 
	local prg_size = console_opts["prg_rom_size_kb"]
	local chr_size = console_opts["chr_rom_size_kb"]
	local dumpfile = process_opts["dump_filename"]

	local filetype = "nes"

--initialize device i/o for NES
	dict.io("IO_RESET")
	dict.io("NES_INIT")

--dump the cart to dumpfile
	if read then

		print("\nDumping PRG & CHR ROMs...")

		--init_mapper()
		
		file = assert(io.open(dumpfile, "wb"))

		--create header: pass open & empty file & rom sizes
		create_header(file, prg_size, chr_size)

		--dump cart into file
		dump_prgrom(file, prg_size, false)
		dump_chrrom(file, chr_size, false)

		--close file
		assert(file:close())
		print("DONE Dumping PRG & CHR ROMs")
	end
end

-- functions other modules are able to call
gxrom.process = process

-- return the module's table
return gxrom